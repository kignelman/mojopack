#!/bin/bash

# Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -eu

function usage {
    echo "Usage: $0 [PROGRAM_PATH] [OPTIONS]"
    echo ""
    echo "This script bundle a dynamically linked program and all it's libraries into a"
    echo "single standalone auto-extractable file."
    echo ""
    echo "OPTIONS"
    echo "    -o | --output  : standalone output path (default: program's name + .sh)"
    echo "    -a | --add     : embed additional files or folders along with the program"
    echo "    -h | --help    : show this help"
}

if [ "$#" == "0" ]; then
    usage
    exit 1
fi

output=""
program_path=$1
adds=()

args=$(getopt -o o:a:h:: --long output:,add:,help:: -- "$@")
eval set -- "$args"
while true ; do
    case "$1" in
        -o|--output)
            output=$2 ; shift 2 ;;
        -a|--add)
            adds+=($2) ; shift 2 ;;
        -h|--help)
            usage; exit 0 ;;
        --)
            break ;;
        *)
            ;;
    esac
done

if [ ! -f "$program_path" ]; then
    echo "Error: Cannot find \"$program_path\""
    echo ""
    usage
    exit 1
fi

ldd "$program_path" &> /dev/null || ( echo "Error: cannot get executable informations from \"$program_path\"" ; exit 1 )

program_name=$(basename "$program_path")
tmp_dir=$(mktemp -d --suffix _mojopack)
tmp_data_dir="$tmp_dir/data"
runner_path="$tmp_data_dir/$program_name.sh"
extract_script="$tmp_dir/run.sh"
ld_linux=$(basename "$(ldd "$program_path" | grep ld-linux | cut -f 1 -d ' ' | xargs)")

# Copy binary dependencies
mkdir -p "$tmp_data_dir" || ( echo "Error: cannot create directory $tmp_data_dir" && exit 1 )
ldd "$program_path" | grep '=>' | cut -d ' ' -f 3 | \
    xargs -I '{}' /bin/bash -c "if [ -f \"{}\" ]; then cp \"{}\" \"$tmp_data_dir\"; fi"
ldd "$program_path" | grep -v '=>' | cut -f 1 -d ' ' | \
    xargs -I '{}' /bin/bash -c "if [ -f \"{}\" ]; then cp \"{}\" \"$tmp_data_dir\"; fi"
cp "$program_path" "$tmp_data_dir"

# Copy additional files or folders
if [ ${#adds[@]} -ne 0 ]; then
    for item in "${adds[@]}"; do
        cp -r "$item" "$tmp_data_dir"
    done
fi

# Build run script
echo "#!/bin/bash
c=\$( dirname \"\$0\" )
\"\$c/$ld_linux\" --library-path \"\$c\" \"\$c/$program_name\" \"\$@\"" >> "$runner_path"
chmod +x "$runner_path"

# Build auto-extractable archive
echo "#!/bin/bash
tmp=\$(mktemp -d --suffix _mojopack)
trap 'rm -rf '\"\$tmp\"' ; exit' INT TERM EXIT
archive_start=\$(( \$(grep -a -m 2 -n __TAR_ARCHIVE_BELOW__ \"\$0\" | tail -n+2 | cut -f 1 -d ':') + 1 ))
tail -n+\"\$archive_start\" \"\$0\" | tar x -C \"\$tmp\"
\"\$tmp/${program_name}.sh\" \"\$@\"
exit \$?
__TAR_ARCHIVE_BELOW__" >> "$extract_script"
tar -O -C "$tmp_data_dir" -c . >> "$extract_script"
chmod +x "$extract_script"

if [ -z "$output" ]; then
    output="$program_name.sh"
fi

if [ -f "$output" ]; then
    echo -n "Destination \"$output\" already exists, overwrite ? (y/N): "
    read -r r
    if [ ! "$r" == "y" ]; then
        echo "Exiting"
        rm -rf "$tmp_dir"
        exit 1
    fi
fi

mv "$extract_script" "$output"
rm -rf "$tmp_dir"

# Dependencies notes:
# - getopt
# - dirname
# - basename
# - pwd
# - rm
# - mkdir
# - ldd
# - cut
# - cp
# - grep
# - tar
# - tail
# - mktemp
# - chmod
# - mv
# - xargs

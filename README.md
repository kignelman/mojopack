# Mojopack

```
Usage: ./mojopack.sh [PROGRAM_PATH] [OPTIONS]

This script bundle a dynamically linked program and all it's libraries into a
single standalone auto-extractable file.

OPTIONS
    -o | --output  : standalone output path (default: program's name + .sh)
    -a | --add     : embed additional files or folders along with the program
    -h | --help    : show this help
```

# Example
```
$ ./mojopack.sh /bin/ls
$ ./ls.sh
ls.sh  mojopack.sh  README.md  LICENSE
```

# License

> Copyright (c) 2017, Jérôme Jutteau <j.jutteau@gmail.com>
>
> This work is open source software, licensed under the terms of the
> BSD license as described in the LICENSE file in the top-level directory.
